const axios = require('axios');

// export async function get(){
//   console.log("I am in get")
//   return fetch("http://127.0.0.1:5000/hej",{
//     method: 'GET',
//     headers: '',
//   }).then(res => {
//     console.log("HEJ")
//     res.json()
//   })
// }

export async function get(){
  console.log("trying to get")
  return axios.get("http://localhost:5000/hej", {headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }}).then(res => res)
}

export async function post(body){
  console.log("trying to post")
  return axios.post("http://localhost:5000/getSomething", {headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  body: body}).then(res => res)
}


// export function post(){
//   fetch('http://127.0.0.1:5000/getSomething', {
//     method: 'POST',
//     headers: '',
//     body: {hej: "hej"}
//   }).then(res => res)
// }
