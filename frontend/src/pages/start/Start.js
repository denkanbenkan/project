import React from 'react';
import { Input, Button } from 'antd';
import {get, post} from '../../helpers/service'

const { TextArea } = Input;



export class Start extends React.Component{
state = {
  inputText: "",
  returnText: {}
}

handleChange = ({target:{value}}) => {
  this.setState({inputText: value})
}

handleSubmit = async () => {
  let body = this.state.inputText
  let returnText = await post(body)
  returnText = returnText.data
  console.log(returnText)
  this.setState({returnText})
}

printString = () => {
    let returnText = Object.assign({}, this.state.returnText)
    let newText = Object.keys(this.state.returnText).reverse().map(item => <p>{this.state.returnText[item] + "\xa0\xa0\xa0" + item.slice(0,4) + " %" }</p>
    )
    return newText
}

  render () {
    return <div>
      <TextArea rows={4} onChange={this.handleChange}/>
      <Button onClick={this.handleSubmit}>Submit</Button>
      {this.printString()}
    </div>
  }
}
export default Start;
