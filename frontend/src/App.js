import React from 'react';
import './App.css';
import {Menu} from 'antd';
import {Start} from './pages/start/Start.js';
import {HashRouter, Route, Link, Switch} from 'react-router-dom';

class App extends React.Component{
  state =  {
    current : "Start"
  }

  handleClick = ({key}) => {
    this.setState({current: key})
  }

  render () { return (
    <HashRouter>
    <div>

      <Menu mode="horizontal" theme="dark" selectedKeys={[this.state.current]} onClick={this.handleClick}>
      <Menu.Item key="Start">
        <Link to="/" >Start</Link>
      </Menu.Item>
    </Menu>

    <Switch>
      <Route path="/" component={Start} ></Route>
    </Switch>

      </div>
    </HashRouter>
    //<h1>Hej på dig Theresa</h1>
  )}
}

export default App;
