import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.datasets import reuters
from keras.utils.np_utils import to_categorical
import numpy as np
from tensorflow.keras import models
from tensorflow.keras import layers
from tensorflow.keras import optimizers
from tensorflow.keras import losses
from tensorflow.keras import metrics
import operator
import os
import regex as re


model = tf.keras.models.load_model(".\\my_model2.h5")


#word_index = reuters.get_word_index()
#word_index = dict(word_index.items())

dic = {}

def readInDic():
    inv_cat = {}
    with open("dicCat.txt", encoding = 'utf-8') as f: 
        for line in f:    
            l = line.strip().split(":")
            inv_cat[l[1]] = l[0]
        return inv_cat


def vectorize_sequences(sequences, dimension=50000):
    # Create an all-zero matrix of shape (len(sequences), dimension)
    results = np.zeros((len(sequences), dimension))
    print(len(results))
    for i, sequence in enumerate(sequences):
        print(i, sequence)
        results[i, sequence] = 1.  # set specific indices of results[i] to 1s
    return results

import regex as re


def tokenizeText(text, word_index):
    text = text.lower()
    text = re.findall(r"\p{L}+", text) # [[\p{L}][^\n\t\.\,!?' '\(\)\/\:\*\"]*
    list = []
    for word in text:
        if word in word_index:
            if word_index[word] < 50000:
                list.append(word_index[word])

    print(list)
    return list

def printTopN(predictions, n):
    text = {}
    dic = readInDic()
    print(dic)
    for i in range(n):
        text[100*predictions[0][np.argmax(predictions)]] = dic[str(np.argmax(predictions))]
        predictions[0][np.argmax(predictions)] = 0
    return text

def pre(text, myDict):
    text = tokenizeText(text, myDict)
    x_test = vectorize_sequences([text])
    predictions = model.predict(x_test)

    print("UNDER ÄR PREDICTIONS")
    print(predictions[0])
    return printTopN(predictions, 3)
