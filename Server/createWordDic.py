import os
import regex as re
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.datasets import reuters
from keras.utils.np_utils import to_categorical
import numpy as np
from tensorflow.keras import models
from tensorflow.keras import layers
from tensorflow.keras import optimizers
from tensorflow.keras import losses
from tensorflow.keras import metrics
import operator
from sklearn.metrics import confusion_matrix
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
from sklearn.metrics import classification_report, accuracy_score, f1_score, precision_score, recall_score
#from tensorflow.compat.v2.math import confusion_matrix

myDict = {}

def tokenize(text):
    text = re.findall(r"\p{L}+", text) # [[\p{L}][^\n\t\.\,!?' '\(\)\/\:\*\"]*
    return text

def readInData():
    lengi = 0
    lengy = 0
    dir_path = os.path.dirname(os.path.realpath(__file__)) # needed for Windows
    arendetyp = set()
    texts = []
    categories = []
    for root, dirs, files in os.walk(dir_path + "/../Backend/Malmo/" + "/data"):
        for file in files:
            with open(dir_path+ "/../Backend/Malmo/" + "/data" + "/kundtjänstdata 2006-2018/" +file, encoding = 'utf-8') as f:
                size = len(f.readline().strip().split(";")) 
                print("------SIZE---------")
                print(size)
                for line in f:
                    l = line.strip().split(";")
                    if(l[1] == "Allmänheten"):
                        if(len(l) == size):
                            texts.append(l[4].lower())
                            categories.append(l[11].lower())
                            arendetyp.add(l[11].lower())
    return arendetyp, texts, categories

setOfArendeType, listOfTexts, listOfCategories = readInData()

def create_word_dic(l):
    for word in l:
        if word in myDict:
            myDict[word] = myDict[word]+1
        else:
            myDict[word] = 1



def getWordDic():
    setOfArendeType, listOfTexts, listOfCategories = readInData()

    for t in listOfTexts:
        create_word_dic(tokenize(t.lower()))
    myD = sorted(myDict.items(), key=operator.itemgetter(1), reverse=True)
    dic = {}
    for i in range(0, len(myD)):
        dic[myD[i][0]] = i
    print(dic)
    return dic




