import pandas
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
from sklearn.metrics import classification_report, accuracy_score
import os
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import LinearSVC
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import classification_report, accuracy_score, f1_score, precision_score, recall_score, confusion_matrix

#DATA_FOLDER = "C:\Users\FGK\Documents\EDAN70\p\Backend\Malmo\data\kundtjänstdata 2006-2018"

def readInData():
    lengi = 0
    lengy = 0
    dir_path = os.path.dirname(os.path.realpath(__file__)) # needed for Windows
    arendetyp = set()
    texts = []
    categories = []
    for root, dirs, files in os.walk(dir_path + "/data"):
        for file in files:
            print(dirs)
            # pandas.read_csv(delimeter=';')
            # set(pandas['arendetyp'])
            with open(dir_path+ "/data" + "/kundtjänstdata 2006-2018/" +file, encoding = 'utf-8') as f:
                size = len(f.readline().strip().split(";")) 
                print("------SIZE---------")
                print(size)
                for line in f:
                    l = line.strip().split(";")
                    if(l[1] == "Allmänheten"):
                        if(len(l) == size):
                            texts.append(l[4].lower())
                            categories.append(l[11].lower())
                            arendetyp.add(l[11].lower())
    return arendetyp, texts, categories

def convert(l):
    dic = {"offentlig miljö": "stadsliv", "skadat": "skadat/saknas", "saknas": "skadat/saknas", "övrigt": "annat",
          "behöver tömmas": "behöver tömmas/skräpigt", "trästaket/slanstaket": "trästaket/slanstaket/planteringsstaket",
          "skräp": "skräp/glas/löv", }
    for i in range(0,len(l)):
        if l[i] in dic:
            l[i] = dic[l[i]]
    return l

def getTopThree(l):
    l2 = sorted(l, reverse=True)[:3]
    l = [i for i in range(0, len(l)) if l[i] in l2]
    return l

if __name__ == '__main__':
    swe_stop = stopwords.words('swedish')
    
    targets, X, y = readInData()
    tfidf = TfidfVectorizer(stop_words=swe_stop, ngram_range=(1,2)) # TODO remove stop
    #tfidf = TfidfVectorizer(ngram_range=(1,2))
    
    X = tfidf.fit_transform(X) 
    y = convert(y)
    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=42)

    #clf = LinearSVC()
    clf = MLPClassifier(max_iter=10, hidden_layer_sizes=(64,64))
    clf.fit(X_train, y_train)

    y_pred = clf.predict(X_val)

    w = open('LinearRegression/withoutTFIDF.txt', "w+",  encoding = 'utf-8')
    w.write(classification_report(y_val, y_pred))

    print(y_pred)

    #temp = 0
    #for i in range(0, len(y_pred)):
    #    l = getTopThree(y_pred[i])
    #    if y_test[i] in l:
    #        temp += 1
    #print(temp/len(y_pred))
    #w.write("Top three - " + str(temp/len(y_pred)) + "\n")
        
    print(classification_report(y_val, y_pred))
    print(accuracy_score(y_val, y_pred))

