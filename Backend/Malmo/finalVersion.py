import os
import regex as re
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.datasets import reuters
from keras.utils.np_utils import to_categorical
import numpy as np
import operator
from tensorflow.keras import models
from tensorflow.keras import layers
from tensorflow.keras import optimizers
from tensorflow.keras import losses
from tensorflow.keras import metrics
from tensorflow.keras.preprocessing.sequence import pad_sequences
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score, f1_score, precision_score, recall_score



def tokenize(text):
    text = re.findall(r"\p{L}+", text) # [[\p{L}][^\n\t\.\,!?' '\(\)\/\:\*\"]*
    return text

def readInData():
    lengi = 0
    lengy = 0
    dir_path = os.path.dirname(os.path.realpath(__file__)) # needed for Windows
    arendetyp = set()
    texts = []
    categories = []
    for root, dirs, files in os.walk(dir_path + "/data"):
        for file in files:
            print(dirs)
            with open(dir_path+ "/data" + "/kundtjänstdata 2006-2018/" +file, encoding = 'utf-8') as f:
                size = len(f.readline().strip().split(";")) 
                print("------SIZE---------")
                print(size)
                for line in f:
                    l = line.strip().split(";")
                    if(l[1] == "Allmänheten"):
                        if(len(l) == size):
                            texts.append(l[4].lower())
                            categories.append(l[11].lower())
                            arendetyp.add(l[11].lower())
    return arendetyp, texts, categories

setOfArendeType, listOfTexts, listOfCategories = readInData()

def textAsDigits(l, d):
    temp = []
    for text in l:
        t = []
        for w in tokenize(text.lower()):
            t.append(d.get(w))
        temp.append(t)
    return temp

def catAsDigitz(l, d):
    temp = []
    for cat in l:
        temp.append(d.get(cat))
    return temp    


def preProcessBaseline():
    cat = dict(enumerate(setOfArendeType))
    inv_cat = {v: k for k, v in cat.items()}
    digitizedCat = catAsDigitz(listOfCategories, inv_cat)

    text = ""
    words = set()
    for t in listOfTexts:
        words.update(tokenize(t.lower()))
    word = dict(enumerate(words))
    inv_map = {v: k for k, v in word.items()}
    digitizedTexts = textAsDigits(listOfTexts, inv_map)
    return digitizedCat, digitizedTexts




# Här börjar modellen, som newsWireTest
dimen = 50000
def vectorize_sequences(sequences, dimension=dimen):
    # Create an all-zero matrix of shape (len(sequences), dimension)
    results = np.zeros((len(sequences), dimension), dtype="float32")
    for i, sequence in enumerate(sequences):
        results[i, sequence] = 1.  # set specific indices of results[i] to 1s
    return results

def to_one_hot(labels, dimension=47):
    results = np.zeros((len(labels), dimension))
    for i, label in enumerate(labels):
        results[i, label] = 1.
    return results

def removeOverK(list):
    return [int(x) for x in list if int(x) < dimen]

def preBoW(dt, digitizedCat):
    x_train, x_test, y_train, y_test = train_test_split(dt, digitizedCat, test_size=0.2, random_state=42)
    # Our vectorized training data
    x_train = vectorize_sequences(x_train)
    # Our vectorized test data
    x_test = vectorize_sequences(x_test)
    # Our vectorized training labels
    y_train = to_one_hot(y_train)
    # Our vectorized test labels
    y_test = to_one_hot(y_test)
    return x_train, x_test, y_train, y_test

def postModel(x_train, x_test, y_train, y_test):
    print("model decided")
    x_val = x_train[:1000]
    partial_x_train = x_train[1000:]
    y_val = y_train[:1000]
    partial_y_train = y_train[1000:]
    model.fit(partial_x_train,
                        partial_y_train,
                        epochs=1,
                        batch_size=512,
                        validation_data=(x_val, y_val))

    print("model fitted")
    results = model.evaluate(x_test, y_test)
    print("model evaluated")
    print("Results:")
    print(results)
    #model.save('./models/realbaseline.h5')
    predictions = model.predict(x_test)
    labels = list(set(listOfCategories))
    pred = [np.argmax(x) for x in predictions]
    y_test = [np.argmax(x) for x in y_test]
    for i in range(0, 52):
        if i not in y_test:
            print("--------" + str(i) + "---------------" )
            y_test.append(i)
            pred.append(i)
    labels = [i for i in range(0,52)]

    cm = confusion_matrix(y_test, pred, labels)
    cr = classification_report(y_test, pred, labels)
    print(cm)
    print(cr)
    w = open("./models/CM.txt", 'w+')
    text = "\t"
    for i in range(0,52):
        text += str(i) + "\t" 
    w.write(text + "\tsum" + "\n\n")

    for i in range(0, 52):
        text = str(i) + " - \t"
        for j in range(0, len(cm[i])):
            if(i == j):
                text += "[" +str(cm[i][j]) + "]\t"
            else:
                text += str(cm[i][j]) + "\t"
        w.write(text + "\t" + str(sum(cm[i])) + "\n")#+ "\n" + str(cm[i][i]) + "/" + str(sum(cm[i])) +  " - " + str((cm[i][i])/sum(cm[i])*100) + "% - " + "row:" + str(i))
    #i=0
    #dic = {v: k for k, v in dic.items()}
    w = open("./models/CR.txt", "w+")
    #w.write(cr)
    #Get top N percentage
    w = open("./models/results.txt", "w+")
    def getTopThree(l):
        l2 = sorted(l, reverse=True)[:3]
        l = [i for i in range(0, len(l)) if l[i] in l2]
        return l

    temp = 0
    for i in range(0, len(predictions)):
        l = getTopThree(predictions[i])
        if y_test[i] in l:
            temp += 1
    print(temp/len(predictions))
    w.write("Top three - " + str(temp/len(predictions)) + "\n")
    w.write("Results - " + str(results))
    print("done")

#Baseline
#digitizedCat, digitizedTexts = preProcessBaseline()
#dt = []
#for t in digitizedTexts:
#    dt.append(removeOverK(t))
#x_train, x_test, y_train, y_test = preBoW(dt, digitizedCat)
#model = models.Sequential()
#model.add(layers.Dense(64, activation='relu', input_shape=(dimen,)))
#model.add(layers.Dense(64, activation='relu'))
#model.add(layers.Dense(52, activation='softmax'))
#model.compile(optimizer='rmsprop',
#              loss='categorical_crossentropy',
#              metrics=['accuracy'])
#postModel(x_train, x_test, y_train, y_test)

#Baseline done

def preProcessBoW1():
    digitizedCat, digitizedTexts = preProcessBaseline()
    inv_cat = {}
    #with open("dicCat.txt", encoding = 'utf-8') as f: 
    with open("dicCat.txt", encoding = 'utf-8') as f: 
            for line in f:    
                l = line.strip().split(":")
                inv_cat[l[0]] = int(l[1])
    digitizedCat = catAsDigitz(listOfCategories, inv_cat)
    return digitizedCat, digitizedTexts

    

#BoW 1
#digitizedCat, digitizedTexts = preProcessBoW1()
#dt = []
#for t in digitizedTexts:
#    dt.append(removeOverK(t))
#x_train, x_test, y_train, y_test = preBoW(dt, digitizedCat)
#model = models.Sequential()
#model.add(layers.Dense(64, activation='relu', input_shape=(dimen,)))
#model.add(layers.Dense(64, activation='relu'))
#model.add(layers.Dense(47, activation='softmax'))
#model.compile(optimizer='rmsprop',
#              loss='categorical_crossentropy',
#              metrics=['accuracy'])
#postModel(x_train, x_test, y_train, y_test)
#Done

def create_word_dic(listOfText, myDict):
    for t in listOfText:
        t = tokenize(t.lower())
        for word in t:
            if word in myDict:
                myDict[word] = myDict[word]+1
            else:
                myDict[word] = 1
    myDict = sorted(myDict.items(), key=operator.itemgetter(1), reverse=True)
    return myDict

def preProcessBoW2():
    digitizedCat, digitizedTexts = preProcessBoW1()
    myDict = {}
    myDict = create_word_dic(listOfTexts, myDict)#Should be list of texts?
    
    
    dic = {}
    for i in range(0, len(myDict)):
        dic[myDict[i][0]] = i
    
    digitizedTexts = textAsDigits(listOfTexts, dic)
    dt = []
    for t in digitizedTexts:
        dt.append(removeOverK(t))

    positions = []


    # To remove all texts that are of length 0
    for i in range(0, len(dt)):
        if len(dt[i]) <= 0:
            positions.append(i)

    for i in range(0,len(positions)):
        dt.pop(positions[i]-i)
        listOfTexts.pop(positions[i]-i)
        digitizedCat.pop(positions[i]-i)
    return digitizedCat, dt


#BoW 2
#digitizedCat, dt = preProcessBoW2()
#x_train, x_test, y_train, y_test = preBoW(dt, digitizedCat)
#model = models.Sequential()
#model.add(layers.Dense(64, activation='relu', input_shape=(dimen,)))
#model.add(layers.Dense(64, activation='relu'))
#model.add(layers.Dense(47, activation='softmax'))
#model.compile(optimizer='rmsprop',
#              loss='categorical_crossentropy',
#              metrics=['accuracy'])
#postModel(x_train, x_test, y_train, y_test)

def getEmbeddings():
    glove_dir = '/home/ubuntu/data/'

    embeddings_index = {}
    f = open('models/swectors-300dim.txt', encoding = 'utf-8')
    for line in f:
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs
    f.close()   

    myDict = {}
    myDict = create_word_dic(listOfTexts, myDict)#Should be list of texts?
    
    dic = {}
    for i in range(0, len(myDict)):
        dic[myDict[i][0]] = i

    max_words = 90000
    embedding_dim = 300
    temp = 0
    embedding_matrix = np.zeros((max_words, embedding_dim))
    for word, i in dic.items(): # word_index should be all unique wwords of all texts
        embedding_vector = embeddings_index.get(word)
        if i < max_words:
            if embedding_vector is not None:
                temp +=1
                # Words not found in embedding index will be all-zeros.
                embedding_matrix[i] = embedding_vector
            else:
                embedding_matrix[i] = np.random.rand(embedding_dim)
    return embedding_matrix, max_words, embedding_dim

def preProcessRNN():
    digitizedCat, digitizedTexts = preProcessBoW2()
    digitizedTexts = pad_sequences(digitizedTexts, maxlen=70)
    digitizedCat = to_categorical(digitizedCat)
    return digitizedCat, digitizedTexts
#Embeddings
#embedding_matrix, max_words, embedding_dim = getEmbeddings()
#digitizedCat, digitizedTexts = preProcessRNN()
#x_train, x_test, y_train, y_test = train_test_split(digitizedTexts, digitizedCat, test_size=0.2, random_state=42)
#y_train = np.asarray(y_train).astype('float32')
#y_test = np.asarray(y_test).astype('float32')
#model = models.Sequential()
#model.add(layers.Embedding(max_words, embedding_dim, weights=[embedding_matrix], trainable=True, input_length=70))
#model.add(layers.Flatten())
#model.add(layers.Dense(64, activation='relu', input_shape=(300,)))
#model.add(layers.Dense(64, activation='relu'))
#model.add(layers.Dense(47, activation='softmax'))
#model.compile(optimizer='rmsprop',
#            loss='categorical_crossentropy',
#            metrics=['accuracy'])
#postModel(x_train, x_test, y_train, y_test)

#SimpleRNN
#embedding_matrix, max_words, embedding_dim = getEmbeddings()
#digitizedCat, digitizedTexts = preProcessRNN()
#x_train, x_test, y_train, y_test = train_test_split(digitizedTexts, digitizedCat, test_size=0.2, random_state=42)
#y_train = np.asarray(y_train).astype('float32')
#y_test = np.asarray(y_test).astype('float32')
#model = models.Sequential()
#model.add(layers.Embedding(max_words, embedding_dim, weights=[embedding_matrix], trainable=True, input_length=70))
#model.add(layers.Dropout(0.25))
#model.add((layers.SimpleRNN(128, return_sequences=True, recurrent_dropout=0.25)))
#model.add(layers.Dropout(0.25)) 
#model.add((layers.SimpleRNN(128)))
#model.add(layers.Dense(47, activation='softmax'))
#model.compile(optimizer='rmsprop',
#            loss='categorical_crossentropy',
#            metrics=['accuracy'])
#postModel(x_train, x_test, y_train, y_test)

#LSTM
#embedding_matrix, max_words, embedding_dim = getEmbeddings()
#digitizedCat, digitizedTexts = preProcessRNN()
#x_train, x_test, y_train, y_test = train_test_split(digitizedTexts, digitizedCat, test_size=0.2, random_state=42)
#y_train = np.asarray(y_train).astype('float32')
#y_test = np.asarray(y_test).astype('float32')
#model = models.Sequential()
#model.add(layers.Embedding(max_words, embedding_dim, weights=[embedding_matrix], trainable=True, input_length=70))
#model.add(layers.Dropout(0.25))
#model.add((layers.LSTM(128, return_sequences=True, recurrent_dropout=0.25)))
#model.add(layers.Dropout(0.25)) 
#model.add((layers.LSTM(128)))
#model.add(layers.Dense(47, activation='softmax'))
#model.compile(optimizer='rmsprop',
#            loss='categorical_crossentropy',
#            metrics=['accuracy'])
#postModel(x_train, x_test, y_train, y_test)

#Bidirectional LSTM
embedding_matrix, max_words, embedding_dim = getEmbeddings()
digitizedCat, digitizedTexts = preProcessRNN()
x_train, x_test, y_train, y_test = train_test_split(digitizedTexts, digitizedCat, test_size=0.2, random_state=42)
y_train = np.asarray(y_train).astype('float32')
y_test = np.asarray(y_test).astype('float32')
model = models.Sequential()
model.add(layers.Embedding(max_words, embedding_dim, weights=[embedding_matrix], trainable=True, input_length=70))
model.add(layers.Dropout(0.25))
model.add(layers.Bidirectional(layers.LSTM(128, return_sequences=True, recurrent_dropout=0.25)))
model.add(layers.Dropout(0.25)) 
model.add(layers.Bidirectional(layers.LSTM(128)))
model.add(layers.Dense(47, activation='softmax'))
model.compile(optimizer='rmsprop',
            loss='categorical_crossentropy',
            metrics=['accuracy'])
postModel(x_train, x_test, y_train, y_test)





